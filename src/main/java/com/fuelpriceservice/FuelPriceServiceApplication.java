package com.fuelpriceservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuelPriceServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuelPriceServiceApplication.class, args);
	}

}
