package com.fuelpriceservice.controller;

import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fuelpriceservice.model.Price;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/fuel")
public class FuelPriceController {

	private static DecimalFormat df = new DecimalFormat("0.00");

	@GetMapping("/price/{city}")
	public ResponseEntity<Price> getFuelPrice(@PathVariable String city) {
		log.info(":: Received request for fuel price of city : " + city);
		Price price = new Price();
		price.setPrice(Double.parseDouble(df.format(ThreadLocalRandom.current().nextDouble(70.00, 90.00))));
		return new ResponseEntity<>(price, HttpStatus.OK);

	}

}
